= Opgaver med trigonometriske funktioner
:page-layout: post
:stem: latexmath

== Ligninger med stem:[\cos(x) = a] og lignende
=== Eksempler
==== Eksempel 1
Der bruges her 2 decimaler. Et eksempel på en opgave kunne være:

_Find alle løsninger til ligningen:_
[stem]
++++
\cos(x) = 0.5
++++

For at isolere stem:[x] bruges den omvendte funktion af stem:[\cos(x)], hvilket er stem:[\cos^{-1}(x)=\arccos(x)], på begge sider af ligningen:

[stem]
++++
\cos^{-1}(\cos(x)) = \cos^{-1}(0.5)
\\
\\
x = \cos^{-1}(0.5)
++++

I Maple findes denne værdi ved at skrive kommandoen:

[stem]
++++
\arccos(0.5)
++++

Og dette giver at stem:[\cos^{-1}(0.5)=1.05] - dette er den første løsning til ligningen. Den anden løsningen findes ved:

[stem]
++++
2\pi - 1.05 = 5.23
++++

Da stem:[\cos(x)] er periodisk med stem:[2\pi] kan alle løsninger skrives på formen:

[stem]
++++
x = 1.05 + p \cdot 2\pi, p \in \mathbb{Z}
\\
\\
x = 5.23 + p \cdot 2\pi, p \in \mathbb{Z}
++++


==== Eksempel 2
Der bruges her 2 decimaler. Et eksempel på en opgave kunne være:

_Find alle løsninger til ligningen:_
[stem]
++++
\sin(x) = 0.5
++++

For at isolere stem:[x] bruges den omvendte funktion af stem:[\sin(x)], hvilket er stem:[\sin^{-1}(x)=\arcsin(x)], på begge sider af ligningen:

[stem]
++++
\sin^{-1}(\sin(x)) = \sin^{-1}(0.5)
\\
\\
x = \sin^{-1}(0.5)
++++

I Maple findes denne værdi ved at skrive kommandoen:

[stem]
++++
\arcsin(0.5)
++++

Og dette giver at stem:[\sin^{-1}(0.5)=0.52] - dette er den første løsning til ligningen. Den anden løsningen findes ved:

[stem]
++++
\pi - 0.52 = 2.62
++++

Da stem:[\sin(x)] er periodisk med stem:[2\pi] kan alle løsninger skrives på formen:

[stem]
++++
x = 0.52 + p \cdot 2\pi, p \in \mathbb{Z}
\\
\\
x = 2.62 + p \cdot 2\pi, p \in \mathbb{Z}
++++


==== Eksempel 3
Der bruges her 2 decimaler. Et eksempel på en opgave kunne være:

_Find alle løsninger til ligningen:_
[stem]
++++
\tan(x) = 0.5
++++

For at isolere stem:[x] bruges den omvendte funktion af stem:[\tan(x)], hvilket er stem:[\tan^{-1}(x)=\arctan(x)], på begge sider af ligningen:

[stem]
++++
\tan^{-1}(\tan(x)) = \tan^{-1}(0.5)
\\
\\
x = \tan^{-1}(0.5)
++++

I Maple findes denne værdi ved at skrive kommandoen:

[stem]
++++
\arctan(0.5)
++++

Og dette giver at stem:[\tan^{-1}(0.5)=0.46] - dette er den første løsning til ligningen. Da stem:[\tan(x)] er periodisk med stem:[\pi] kan alle løsninger skrives på formen:

[stem]
++++
x = 0.46 + p \cdot \pi, p \in \mathbb{Z}
++++


==== Eksempel 4
Der bruges her 2 decimaler. Et eksempel på en opgave kunne være:

_Find alle løsninger til ligningen i intervallet stem:[2] til stem:[5]:_
[stem]
++++
\sin(3x) = \frac{\sqrt{2}}{2}
++++

Først isoleres stem:[x] ved at bruge stem:[\sin^{-1}(x)] på begge sider af lighedstegnet:

[stem]
++++
\sin^{-1}(\sin(3x)) = \sin^{-1}\left(\frac{\sqrt{2}}{2}\right)
\\
\\
3x = \sin^{-1}\left(\frac{\sqrt{2}}{2}\right) = \frac{\pi}{4}
\\
\\
x = \frac{\pi}{12} = 0.26
++++

Den anden løsning findes ved:

[stem]
++++
\pi - 0.26 = 2.88
++++

stem:[\sin(x)] er periodisk med stem:[2\pi], men det er stem:[\sin(3x)] _ikke_. Perioden stem:[T] for stem:[\sin(bx)] er givet ved:

[stem]
++++
T = \frac{2\pi}{b}
++++

Det betyder altså at perioden for stem:[\sin(3x)] er stem:[T = \frac{2\pi}{b}=\frac{2\pi}{3}=2.09]. De andre løsninger findes ved at ligge perioden til de to første løsninger:

[stem]
++++
0.26 + 2.09 = 2.35
\\
\\
0.26 + 2 \cdot 2.09 = 4.44
++++

For stem:[3 \cdot 2.09] bliver resultatet større end stem:[5], så derfor skal det ikke med. Og for den anden løsning:

[stem]
++++
2.88 + 2.09 = 4.97
++++

For stem:[2 \cdot 2.09] bliver resultatet større end stem:[5], så derfor skal det ikke med. Det betyder at løsningerne til ligningen er:

[stem]
++++
x = 2.88
\\
\\
x = 2.35
\\
\\
x = 4.44
\\
\\
x = 4.97
++++


=== Opgaver
* Løs lignigen stem:[\cos(x) = 0.2]
* Løs ligningen stem:[\cos(3x) = 0.8] i intervallet stem:[0] til stem:[10]
* Løs ligningen stem:[\sin\left( \frac{x}{2} \right) = 0.12]
* Løs ligningen stem:[\sin(7x) = \frac{\sqrt{2}}{2}]
* Løs ligningen stem:[\tan\left( \frac{x}{3} \right) = 12]
* Løs ligningen stem:[\tan(5x) = 3] i intervallet stem:[0] til stem:[5]


== Harmoniske svingninger
=== Eksempler
==== Eksempel 1
Et eksempel på en opgave kunne være:

_Bestem udgangsniveau, amplitude, periode, frekvensen og faseforskydning for følgende funktion:_

[stem]
++++
f(x)=5\sin(8x)+3
++++

Den generelle harmoniske svingning er givet ved:

[stem]
++++
f(x) = A\sin(bx + c) + d
++++

Her er stem:[A] amplituden, perioden stem:[T=\frac{2\pi}{b}], stem:[d] er udgangsniveauet, stem:[c] er fasekonstanten og frekvensen er givet ved stem:[f=\frac{1}{T}]. Faseforskydningen er stem:[-\frac{c}{b}].

Det betyder i vores eksempel at udgangsniveauet er stem:[d=3], amplituden er stem:[A=5], perioden er stem:[T=\frac{2\pi}{8}=\frac{\pi}{4}], frekvensen er stem:[f=\frac{1}{\frac{\pi}{4}}=\frac{4}{\pi}] og faseforskydningen er stem:[-\frac{0}{8}=0].

==== Eksempel 2 WORK IN PROGRESS
image::opgaver-trigonometri-find-f.png[Ya]

=== Opgaver
* Bestem udgangsniveau, amplitude, periode, frekvens og faseforskydning for stem:[f(x)=\sin(x) + 2]
* Bestem udgangsniveau, amplitude, periode, frekvens og faseforskydning for stem:[f(x)=\frac{1}{2}\sin(\pi \cdot x + 3) + 5]
* Bestem udgangsniveau, amplitude, periode, frekvens og faseforskydning for stem:[f(x)=3\sin(\frac{1}{2}x + 1)]
