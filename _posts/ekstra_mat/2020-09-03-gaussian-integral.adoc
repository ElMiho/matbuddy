= Evaluér stem:[\int_{0}^{\infty}e^{-x^2} dx]
:page-layout: post
:stem: latexmath

Målet er at evaluere:

[stem]
++++
I = \int_{0}^{\infty} e^{-x^2} dx
++++

Først ses der at stem:[I] også er lig:

[stem]
++++
I = \int_{0}^{\infty} e^{-y^2} dy
++++

Dette skyldes, at om man integrerer med hensyn til stem:[x] eller stem:[y] er ligegyldigt. Det betyder derfor at:

[stem]
++++
I^{2} = \int_{0}^{\infty} e^{-x^2} dx \int_{0}^{\infty} e^{-y^2} dy = \int_{0}^{\infty} \int_{0}^{\infty} e^{-x^2 - y^2} dx~dy
++++

Denne omskrevning er mulig, da alt der har noget med stem:[x] at gøre er en konstant i forhold til stem:[y$.

Herefter laves der substitutionen stem:[t = \frac{x}{y}], hvilket betyder at stem:[x = ty] - altså stem:[x(t)=ty]. Dette betyder også at:

[stem]
++++
dx = y~dt
++++

Efter enhver substitution skal der også ses på grænserne, hvilket giver:

[stem]
++++
t = \frac{0}{y} = 0
++++

Samt:

[stem]
++++
t = \lim_{n \to \infty}\frac{n}{y} = \infty
++++

Alt dette betyder, ift. substitutionen, at:

[stem]
++++
I^{2} = \int_{0}^{\infty} \int_{0}^{\infty} ye^{-y^{2}t^{2} - y^{2}} dt~dy = \int_{0}^{\infty} \int_{0}^{\infty} ye^{-y^{2}(t^{2} + 1)} dy~dt
++++

Som der ses blev stem:[dt~dy] omskrevet til stem:[dy~dt], hvilket skyldes noget argumentation fra universitetet.

Det inderste integral er:

[stem]
++++
\int_{0}^{\infty} ye^{-y^{2}(t^2 + 1)} dy
++++

Dette er muligt at evaluere, ved at lave substitutionen stem:[u = y^2(t^2 + 1)], hvilket giver:

[stem]
++++
du = 2y(t^2 + 1)~dy \implies y~dy = \frac{1}{2(t^2 + 1)} du
++++

Grænserne er det samme:

[stem]
++++
u = 0^2(t^2 + 1) = 0
++++

[stem]
++++
u = \lim_{n \to \infty} n^2(t^2 + 1) = \infty
++++

Det vil sige, at det inderste integral bliver til:

[stem]
++++
\int_{0}^{\infty} \frac{e^{-u}}{2(t^2 + 1)} du = \frac{1}{2(t^2 + 1)} \int_{0}^{\infty} e^{-u} du
++++

Dette giver:

[stem]
++++
\frac{1}{2(t^2 + 1)} (\lim_{n \to \infty}-e^{-n} + e^{-0}) = \frac{1}{2(t^2 + 1)}
++++

Indsættes dette i stem:[I^2] fås der følgende integral:

[stem]
++++
I^2 = \int_{0}^{\infty} \frac{1}{2(t^2 + 1)} dt
++++

For at evaluere dette integral ses der på:

[stem]
++++
y(x) = \arctan(x)
++++

Her er stem:[\arctan(x)], den omvendte funktion af stem:[\tan(x)] - det vil sige at stem:[\tan(\arctan(x)) = x]. Der ønskes at finde stem:[y'(x)], hvilket først gøres ved at bruge stem:[\tan(x)] på begge side:

[stem]
++++
\tan(y(x)) = x
++++

Differentieres begge sider med hensyn til stem:[x] fås der på højre side:

[stem]
++++
\frac{d}{dx} x = 1
++++

På venstre side er der tale om en sammensat funktion, hvilket vil sige at kædereglen skal bruges:

[stem]
++++
\frac{d}{dx} f(g(x)) = f'(g(x))g'(x)
++++

Det betyder altså at venstre side bliver til:

[stem]
++++
\frac{d}{dx}\tan(y(x)) = y'(x)(1+\tan^2(y))
++++

Dette skyldes at stem:[\frac{d}{dx} \tan(x) = 1 + \tan^2(x)]. Sammenholdes højre og venstre side fås der:

[stem]
++++
y'(x)(1+\tan^2(y(x))) = 1
++++

Det betyder at:

[stem]
++++
y'(x) = \frac{1}{1+ \tan^2(y(x))} = \frac{1}{1+x^2}
++++

Det vil sige at stem:[I^2] bliver til:

[stem]
++++
I^2 = \frac{1}{2}\int_{0}^{\infty} \frac{1}{t^2 + 1} dt = \frac{1}{2} (\arctan(\infty) - \arctan(0)) = \frac{1}{2} \frac{\pi}{2} = \frac{\pi}{4}
++++

Dette skyldes da stem:[\arctan(\infty) = \frac{\pi}{2}] samt at stem:[\arctan(0) = 0]. Da stem:[I] er postiv, betyder det at:

[stem]
++++
I = \int_{0}^{\infty} e^{-x^2} dx = \frac{\sqrt{\pi}}{2}
++++
