---
layout: page
title: Opgaver
permalink: /opgaver/
---

Her er en liste med forskellige opgaver:

* https://matbuddy.dk/2020/09/29/opgaver-trigonometri.html[Opgaver med trigonometri]

* https://matbuddy.dk/2020/11/02/opgaver-integralregning-klassiske.html[Opgaver med integralregning - klassiske]

* https://matbuddy.dk/2020/12/01/opgaver-integralregning-integrationsproeve.html[Opgaver med integralregning - integrationsprøve]

* https://matbuddy.dk/2020/12/01/opgaver-integralregning-substitution.html[Opgaver med integralregning - substitution]

* https://matbuddy.dk/2020/12/01/opgaver-integralregning-partiel.html[Opgaver med integralregning - partiel (gange)]

* https://matbuddy.dk/2020/12/01/opgaver-integralregning-ser-svaere-ud.html[Opgaver med integralregning - ser svære ud]

* https://matbuddy.dk/2020/11/03/opgaver-differentialligninger.html[Opgaver med differentialligninger]